//
// Created by denis svinarchuk on 16.02.2018.
//

#include "../krmdspq/DispatchQueue.h"

#define BOOST_TEST_THREAD_SAFE 1
#define BOOST_TEST_MODULE main_thread_async

#include <thread>
#include <boost/test/included/unit_test.hpp>

BOOST_AUTO_TEST_CASE( test_init )
{
    auto main_thread_id = std::this_thread::get_id();

    for (int i = 0; i < 100 ; ++i) {
        dispatch::Queue::main()->async([main_thread_id,i]{
            BOOST_CHECK_EQUAL(std::this_thread::get_id(), main_thread_id );
            if (i==99) {
                dispatch::main::exit();
           }
        });
    }

    dispatch::main::loop([main_thread_id]{
        BOOST_CHECK_EQUAL(std::this_thread::get_id(), main_thread_id );
        sleep(1);
    });
}
